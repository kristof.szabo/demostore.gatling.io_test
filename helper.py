def is_item_outside_of_element(contentScrollHeight, contentScrollWidth, contentLocation, containerSize, containerLocation):
    contentRightEdge = contentScrollWidth+ contentLocation['x']
    containerRightEdge = containerSize['width'] + containerLocation['x']

    contentBottomEdge = contentScrollHeight + contentLocation['y']
    containerBottomEdge = containerSize['height'] + contentLocation['y']

    return contentBottomEdge > containerBottomEdge or contentLocation['y'] < containerLocation['y'] or contentLocation['x']< containerLocation['x'] or contentRightEdge > containerRightEdge
