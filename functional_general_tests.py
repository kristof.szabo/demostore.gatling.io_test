
import unittest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from parameterized import parameterized

from driver_creator import ChromeDriver

class GeneralTest(unittest.TestCase):
    @parameterized.expand([("Chrome", ChromeDriver(1920,1080).get_driver())])
    def test_error_page_has_go_home_button(self, name, driver):
        driver.get("http://demostore.gatling.io/valamicske")
        homeButton = driver.find_element(By.XPATH,"//body/a[@href='/']")
        self.assertTrue(homeButton.get_attribute("href") == "http://demostore.gatling.io/")

if __name__ == "__main__":
    unittest.main()
