import unittest

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from driver_creator import ChromeDriver
from helper import is_item_outside_of_element

from parameterized import parameterized

 # Nem lehet ugyan azt a drivert használni, mert előbb ugyanarra az oldalra fognak hivatkozni \\ testParams=[("Small", ChromeDriver(360,740).get_driver())]
class MinicartTest(unittest.TestCase):


    """
    Teszt indokoltsága
        Nem lóghat ki konténeren kívülre a gomb
    Teszt sikere
        A Cart View button benne van a kategória konténerben lévő cart konténerben.
    
    """
    @parameterized.expand([("Small", ChromeDriver(360,740).get_driver())])
    def test_cart_view_button_inside_cart_box(self, name, driver):
        driver.get("http://demostore.gatling.io/category/all")
        addToCartButton = driver.find_element(By.XPATH, "//h2[@class='display-3 mb-5']/../div/div/div/p/a[@class='btn btn-primary addToCart']")
        addToCartButton.send_keys(Keys.RETURN)
        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//div[@class='cart cartInactive']/p/a"))
        )

        cartContainer = driver.find_element(By.CSS_SELECTOR, "div.cartInactive")

        viewCartButton = driver.find_element(By.XPATH, "//div[@class='cart cartInactive']/p/a[@href='/cart/view']")
        
        viewCartButtonScrollHeight = driver.execute_script("return document.querySelector('.cartInactive>p>a[href=\"/cart/view\"]').scrollHeight")
        viewCartButtonScrollWidth = driver.execute_script("return document.querySelector('.cartInactive>p>a[href=\"/cart/view\"]').scrollWidth")

        self.assertFalse(is_item_outside_of_element(viewCartButtonScrollHeight, viewCartButtonScrollWidth, viewCartButton.location, cartContainer.size, cartContainer.location))
        
    """
    Teszt indokoltsága
        Nem lóghat ki konténeren kívülre cart bármelyik gombja
    Teszt sikere
        A Cart Clear button benne van a kategória konténerben lévő cart konténerben.
    
    """    
    @parameterized.expand([("Small", ChromeDriver(360,740).get_driver())])
    def test_cart_clear_button_inside_cart_container(self, name, driver):
        driver.get("http://demostore.gatling.io/category/all")
        addToCartButton = driver.find_element(By.XPATH, "//h2[@class='display-3 mb-5']/../div/div/div/p/a[@class='btn btn-primary addToCart']")
        addToCartButton.send_keys(Keys.RETURN)
        WebDriverWait(driver,10).until(EC.presence_of_element_located((By.XPATH, "//div[@class='cart cartInactive']/p/a")))

        cartContainer = driver.find_element(By.CSS_SELECTOR, "div.cartInactive")
        clearCartButton = driver.find_element(By.XPATH, "//div[@class='cart cartInactive']/p/a[@href='/cart/clear']")

        clearCartButtonSelector = '.cartInactive>p>a[href=\"/cart/clear\"]'
        clearCartButtonScrollHeight = driver.execute_script("return document.querySelector('" + clearCartButtonSelector +"').scrollHeight")
        clearCartButtonScrollWidth = driver.execute_script("return document.querySelector('" + clearCartButtonSelector +"').scrollWidth")

        self.assertFalse(is_item_outside_of_element(clearCartButtonScrollHeight, clearCartButtonScrollWidth, clearCartButton.location, cartContainer.size, cartContainer.location))

if __name__ == "__main__":
    unittest.main()
       