import unittest
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from parameterized import parameterized

from driver_creator import ChromeDriver

class CartTest(unittest.TestCase):
    @parameterized.expand( [("Chrome", ChromeDriver(1920,1080).get_driver())])
    def test_empty_cart(self, name, driver):
        driver.get("http://demostore.gatling.io")

        try:
            driver.find_element(By.CSS_SELECTOR, "div.cart.cartInactive")
            cartButtons = driver.find_elements(By.XPATH, "//div[@class='cart cartInactive]'/p/a")
        except:
            cartButtons = None
        self.assertIsNone(cartButtons)
    
    @parameterized.expand( [("Chrome", ChromeDriver(1920,1080).get_driver())])
    def test_buy_item_adds_to_cart(self, name, driver):
        driver.get("http://demostore.gatling.io/category/all")

        add_to_cart = driver.find_elements(By.XPATH, "//H2[@class='display-3 mb-5']/../div[@class='row']/div[@class='col-4']/div/p/a")[0]
        add_to_cart.send_keys(Keys.RETURN)
        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//a[@href='/cart/view']"))
        )
        cartValues = driver.find_elements(By.XPATH, "//div[@class='cart cartInactive']/p/span")
        
        self.assertEqual(cartValues[0].get_attribute("innerHTML"), "1")

if __name__ == "__main__":
    unittest.main()
