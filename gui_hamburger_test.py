import unittest

from selenium.webdriver.common.by import By
from parameterized import parameterized

from driver_creator import ChromeDriver

testParams = [("Chrome", ChromeDriver(1920,1080).get_driver())]


"""
Teszt indokoltsága
    Ha a menü elemek nem férnek el össze nyomás esetén
Teszt sikere
    Az oldal szélességének módosítása esetén túl kis méretnél átvált egy gombra és nagynál eltűnteti azt.
"""
class HamburgerTest(unittest.TestCase):
    @parameterized.expand(testParams)
    def test_hamburger_menu_visibility(self,name, driver):
        driver.get("http://demostore.gatling.io")
        togglerButton = driver.find_element(By.XPATH, "//button[@class='navbar-toggler']")
        driver.set_window_size(1920,1080)
        visibleAtBigWidth = togglerButton.value_of_css_property("display") == "none"
        driver.set_window_size(640,480)
        visibleAtSmallWidth = togglerButton.value_of_css_property("display") != "none"
        driver.set_window_size(1920,1080)
        visibleAtBigWidth =  togglerButton.value_of_css_property("display") == "none"
        self.assertTrue(visibleAtSmallWidth & visibleAtBigWidth)

if __name__ == "__main__":
    unittest.main()


