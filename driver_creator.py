from tokenize import Number
from selenium import webdriver

CHROME_DRIVER_PATH = "C:\\WebDrivers\\chromedriver.exe"
EDGE_DRIVER_PATH = "C:\\WebDrivers\\EdgeDriver\\msedgedriver.exe"
class ChromeDriver:
    def __init__(self, width : Number, height : Number) -> None:
        self.width = width
        self.height = height
    
    def get_driver(self) -> webdriver:
        driver = webdriver.Chrome(CHROME_DRIVER_PATH)
        driver.set_window_size(self.width, self.height, )
        return driver

class EdgeDriver:
    def __init__(self, width :Number, height : Number) -> None:
        self.width = width
        self.height = height

    def get_driver(self) -> webdriver:
        driver = webdriver.Edge(EDGE_DRIVER_PATH)
        driver.set_window_size(self.width, self.height)
        return driver

        