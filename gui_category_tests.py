from importlib.resources import path
from turtle import width
import unittest

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from helper import is_item_outside_of_element
from parameterized import parameterized
from driver_creator import ChromeDriver, EdgeDriver

mobile_emulation = { "deviceName": "Samsung Galaxy S8+" }
chrome_options = webdriver.ChromeOptions()
chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)
driver = webdriver.Chrome("C:\\WebDrivers\\chromedriver.exe", options=chrome_options)
testParams = [
        ("Large", EdgeDriver(360,740)),
        ("Large", EdgeDriver(1024,768)),
        ("Large", EdgeDriver(1920,1080)),
        ("Small", driver),
        ("Medium", ChromeDriver(1024, 768).get_driver()),
        ("Large", ChromeDriver(1920, 1080).get_driver())
    ]

class CategoryTest(unittest.TestCase):

    """
    Teszt indokoltsága
	    Ha egy szöveg kilóg a hozzá tartozó konténerbe rontja az oldal olvashatóságát.
    Teszt sikere
	    Nem lóg ki sehol a szöveg a kategória konténerből, amely benne van.
    """
    @parameterized.expand(testParams)
    def test_long(self, name, driver):
        driver.get("http://demostore.gatling.io/category/all")
        
        listItems = driver.find_elements(By.XPATH, "//ul[@class='list-group']/li")
        listItemsTexts = driver.find_elements(By.XPATH, "//ul[@class='list-group']/li/a")
        contentScrollWidth = driver.execute_script("return document.querySelector('.list-group-item>a').scrollWidth")
        contentScrollHeight = driver.execute_script("return document.querySelector('.list-group-item>a').scrollHeight")

        itemTextLocation = listItemsTexts[0].location
        listItemSize = listItems[0].size
        listItemLocation = listItems[0].location
        
        self.assertFalse(is_item_outside_of_element(contentScrollHeight, contentScrollWidth, itemTextLocation, listItemSize, listItemLocation))


    """
    Teszt indokoltsága
        A kategória konténer szélessége nem függhet az oldal tartalmát megjelenítő elemektől. Mivel inkonzisztenciát mutatna az oldal kinézetében.
    Teszt sikere
	    Minden oldalt egyforma a kategória konténer.
    Teszt megvalósítása
	    Végig iterálás az oldalakon összehasonlítva a kategória konténer szélességét.

    """
    @parameterized.expand(testParams)
    def test_is_same_width_at_every_site(self, name, driver):
        sites = ["http://demostore.gatling.io/", "http://demostore.gatling.io/category/all", "http://demostore.gatling.io/login"]
        isFirst = True
        isSameWidth = True
        firstWidth = 0
        for site in sites:
            driver.get(site)
            currentWidth = driver.find_element(By.XPATH, "//ul/li[@class='list-group-item']").size["width"]
            if isFirst:
                firstWidth = currentWidth
            elif firstWidth != currentWidth:
                isSameWidth = False
        self.assertTrue(isSameWidth)
    

    """
    Teszt indokoltsága
        (Deprecated) A kategória oszlopában lévő szöveg nem zavarhatja meg a content rész tartalmát.
    Teszt sikere
	    Nem lóg át a kategória konténer címe a content területére.
    Teszt megvalósítása
        A konténer címének pozíciója és teljes szélessége alapján megvizsgálni, hogy belelóg e a contentbe
    """
    @parameterized.expand(testParams)
    def test_does_content_and_category_not_slips_together(self, name, driver):
        driver.get("http://demostore.gatling.io/")

        categoriesTitle = driver.find_element(By.XPATH, "//h3[@class='display-4']")
        categoriesTitleScrollWidth = driver.execute_script("return document.querySelector('h3.display-4').scrollWidth")
        contentContainer = driver.find_element(By.XPATH, "//div[@class='container-fluid mt-5']/div/div[@class='col-7']")

        self.assertTrue(categoriesTitle.location['x'] + categoriesTitleScrollWidth > contentContainer.location['x'])

if __name__ == '__main__':
    unittest.main()




        

