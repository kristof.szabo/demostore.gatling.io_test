import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup

from parameterized import parameterized

from driver_creator import ChromeDriver
class ViewTest(unittest.TestCase):
        
    @parameterized.expand( [("Chrome", ChromeDriver(1920,1080).get_driver())])
    def test_select_product_shows_view_with_same_details(self, name, driver):
        driver.get("http://demostore.gatling.io/category/all")
        element = driver.find_elements(By.CSS_SELECTOR, ".mt-5>.row>.col-8>.row>.col-4")
        soup = BeautifulSoup(element[0].get_attribute("outerHTML"),'html.parser')
        imageSource = soup.div.img['src']
        productName = [*soup.h4.stripped_strings][0]
        productPrice = soup.div.p.find_next("p").find_next("p").text
        link = driver.find_elements(By.CSS_SELECTOR, ".link-unstyled")
        link[0].click()

        viewName = driver.find_element(By.CSS_SELECTOR, ".display-3.mb-5").text
        viewImage = driver.find_element(By.XPATH, "//div/div/img[@src]").get_attribute("src")
        viewPrice = driver.find_elements(By.XPATH, "//div[@class='col-8']/div")[2].text
        imageMatch = (viewImage.find(imageSource)!=-1)
        priceMatch = (viewPrice.find(productPrice)!=-1)
        self.assertTrue(imageMatch & (viewName == productName) & priceMatch)
    
    @parameterized.expand( [("Chrome", ChromeDriver(1920,1080).get_driver())])
    def test_no_back_button_on_first_page(self, name, driver):
        driver.get("http://demostore.gatling.io/category/all")

        firstButtonInPagination = driver.find_element(By.XPATH, "//div[@class='col-8']/nav[@class='mt-3']/ul/li/a")

        self.assertEqual(firstButtonInPagination.text, "1")
    
    @parameterized.expand( [("Chrome", ChromeDriver(1920,1080).get_driver())])
    def test_no_next_button_on_last_page(self, name, driver):
        driver.get("http://demostore.gatling.io/category/all")
        isStillNext = True

        while True:
            lastButtonInPagination = driver.find_elements(By.XPATH, "//div[@class='col-8']/nav[@class='mt-3']/ul/li/a")[-1]
            if lastButtonInPagination.text != "Next":
                isStillNext = False
                break
        
            lastButtonInPagination.send_keys(Keys.ENTER)
            
        self.assertFalse(isStillNext)

if __name__ == "__main__":
    unittest.main()
    
        
            
            
        


        


        



    