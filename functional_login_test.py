import unittest

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from parameterized import parameterized

from driver_creator import ChromeDriver

USERNAME_FIELD_ID = "username"
PASSWORD_FIELD_ID = "password"
DEFAULT_URL="http://demostore.gatling.io"

class LoginTest(unittest.TestCase):

    @parameterized.expand([("Chrome", ChromeDriver(1920,1080).get_driver())])
    def test_successful_login(self, name, driver):
        testUsername = "admin"
        driver.get(DEFAULT_URL + "/login")
        LoginTest.login(testUsername, "admin", driver)
        try:
            WebDriverWait(driver, 10).until(EC.url_changes(DEFAULT_URL))
        except:
            driver.quit()
        
        spans = driver.find_elements(By.CSS_SELECTOR, "span.text-white")
        textFound = False
        for i in spans:
            if(i.text == ("Hi, " + testUsername)):
                textFound = True
        buttons = driver.find_elements(By.CSS_SELECTOR, "button.btn.btn-secondary")
        buttonFound = False
        for i in buttons:
            if(i.text == "Logout"):
                buttonFound = True
        
        self.assertTrue(textFound & buttonFound)

    @parameterized.expand([("Chrome", ChromeDriver(1920,1080).get_driver())])
    def test_invalid_login(self, name, driver):
        driver.get(DEFAULT_URL + "/login")

        LoginTest.login("adm","adm", driver)
        try:
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, ".alert.alert-danger")))
        except:
            driver.quit()
            
        self.assertIsNotNone(driver.find_element(By.CSS_SELECTOR, ".alert.alert-danger"))
        

    def login(username, password, driver):
        usernameField = driver.find_element(By.ID, USERNAME_FIELD_ID)
        usernameField.send_keys(username)
        passwordField = driver.find_element(By.ID, PASSWORD_FIELD_ID)
        passwordField.send_keys(password)
        passwordField.send_keys(Keys.RETURN)

if __name__ == "__main__":
    unittest.main()